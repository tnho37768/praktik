# Praktik

## Uge 4
Mødte mine "kunder" snakkede om de forskellige projekter og udarbejdede brainstorm.
Landede på projektet hvor jeg skal få kiosken til at virke og integrere den med Schultz fremmøde og ludus.
Om muligt lave noget med ansigtsgenkendelse i stedet for at bruge sundhedskort

## Uge 5
Venter på dokumentation, arbejder i mellemtiden på ansigtsgodkendelse

## Uge 6
Venter på dokumentation, arbejder i mellemtiden på ansigtsgodkendelse

## Uge 7

Har kørt de første forsøg med angsigtsgenkendelse og mener jeg har fundet en vej frem.

## Uge 8

Sidder fast i debugging af face i opencv-contrib

## Uge 9

Forsøgte med forskellige løsninger på debugging problemet

## Uge 10

Opgivet at få face recognition til at virke. Koncentrerer mig om at få det andet til at virke i stedet.

## Uge 11

Arbejder på integrationen med webservice i ludus.

## Uge 12

Påskeferie

## Uge 13

Bon printer integration

## Uge 14
Touch screen driller, kan ikke få opsætningen til at se rigtig ud.